=========
Kanod-kea
=========

.. toctree::
    :maxdepth: 2

Overview
========

Kanod Kea is a container that exposes an ISC Kea DHCP controller whose
configuration is defined by the Baremetal Host and Kanod Networks defined
in the hosting Kubernetes controller.

It is a drop-in replacement for the dnsmask controller of Metal³ Ironic Pod
when only the static configuration is used. If kanod network configuration
capabilities are used, kanod-kea can handle new networks on the fly.

By default, kanod-kea checks the status of all Network custom resources. If a
DHCP config is exposed, it is defined in the Kea server. Kea will also try to
patch the Network resource to set the Dhcp relay target if it is not the IP of
the Kea server.

In BaremetalHost configuration, the daemon also checks the Baremetal Host
known in the environment and will only respond to Pxe request of known hosts.
It can be used in settings where several DHCP server should coexist on the
same network but is usually not useful when separate Pxe Vlans are used.

Kanod Kea static configuration
==============================
An initial static configuration is defined in file ``/config/dhcp.yaml``. This
file is usually defined in a mounted volume (configMap). It holds the initial
static network configuration and follows the Json schema of KanodConfig.

.. jsonschema:: schema.json
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:


Configuration Variables
=======================

CONFIG_FILE:
    Name of the file holding the static configuration. ``/config/dhcp.yaml`` is used
    as default value.

