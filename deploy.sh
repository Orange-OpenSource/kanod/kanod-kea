#!/bin/bash

kubectl patch deployment -n baremetal-operator-system ironic --type=json  -p='[{"op": "remove", "path": "/spec/template/spec/containers/0"}]'
kustomize build . | kubectl apply -f -
kustomize build config/samples | kubectl apply -f -
