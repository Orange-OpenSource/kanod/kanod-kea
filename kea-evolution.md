# Unified handling of DHCP and DHCP less


## Status
Implementable

## Summary
Control the allocation of addresses by DHCP servers from
resources in the cluster.

## Motivation
The use of DHCP is required for provisionning servers that do not support virtualmedia. But DHCP introduces an external database on which Metal3 has limited visibility. The IP assigned to each node is then only available inside the cluster and cannot be used as a paramter in resource deployments.

Although the use of DHCP can be limited to OS provisioning, we still need address pools for this phase that may conflict with provisionned servers operating on the same network.

There are three main modes that should be supported by Metal3/Ironic:

* Static IP address allocation. This is the prefered mode. Addresses are defined within the cluster and pushed throug configuration files, kernel parameters, etc. to the running OS. This approach
is fully handled by the DHCP-less proposal
* Dynamic IP address allocations handled by Ironic. This is the scope of this proposal. For at least
PXE boot we use a DHCP server under the control of Ironic to associate IP addresses to specific
interfaces of the server.
* Dynamic IP address allocated by an external DHCP server. The Ironic DHCP runs in proxyDHCP mode and only offers options for PXE boot but no IP address.


## Proposal
We propose to use information on BareMetalHost to control the lease given back by the DHCP server to this host. 

* IP addresses will be assigned to BareMetalHost resources usually using an in-cluster IPAM to manage them. Those addresses will be availabe to network configuration templater and to the DHCP server
* The DHCP server will dynamically use leases created from the boot mac address and an address associated to the baremetalhost.


### Associating IP address to BareMetalHost
IP addresses will all belong to a preset label domain on the BareMetalHost. The name of the label will be considered as the id of the address.

We will use a preset label to identify BareMetalHosts that are registered on the DHCP service. The value of the label will be the id of the address to use for the lease.

The hardware address for the lease will be the boot mac address of the BareMetalHost.

An annotation of the BareMetalHost resource will describe which IP addresses should be generated for this
host. The value of the annotation will be a map associating an address id to an IpPool. A claim will be
generated for each element of the map, the ip address associated to the id will be the one obtained from
resolving that claim.

### Dynamic leases
We need a DHCP server that provides an API to modify leases dynamically. We will use Kea (https://www.isc.org/kea/).

We will use a custom resource and a controller to operate a specific deployment of the Kea DHCP server. The custom resource will describe the layout of the subnets managed by the Kea deployment.

It may also be associated with a custom template for the configuration of the DHCP4 server to handle features not supported by the standard configuration.

## Implementation details

We will directly register dynamic leases in the DHCP4 server (typically configured to use memfiles). We will use the `dhcp_lease_cmds` Kea plugin to insert or delete the leases. The options associated to a lease are still defined in the configuration of the subnet to which the lease is attached. In fact in a multi-cluster scenario it is important to keep track of the exact subnet associated to a lease and it is probably better to avoid Kea subnet selection procedure.

Even if it is possible to register leases in Kea to allocate an IP address to a hardware address, this has not the same effect as a reservation because leases cannot be used as a gateway mechanism
to limit the access to the DHCP service to known hosts. Unknown hardware address will be associated
to arbitrary IPs in the pool selected by the standard Kea mechanisms.

A small Kea plugin has been developed to disable the creation of new leases by Kea. It uses the notion of Kea hooks to unconditionally answer to skip the `lease4_select` phase. The effect is that
new leases cannot be created by the standard Kea lease creation procedure.

## Alternatives

### Using Kea reservations
Kea reservations are the standard way in Kea to associate an IP address to an hardware address. Two approaches are possible:
* static reservations defined in the dhcp4 configuration file
* dynamic reservations with the host commands plugin.
The main drawback are:
* For dynamic registrations:
    * it requires a new database for dynamic registrations that will be a copy of what is defined in the Kubernetes control plane and a superset of the lease database
    * The host command plugin is an ISC premium library.
* For static registrations:
    * each time a host is changed, an update of the complete configuration must be performed. Although it is significantly faster than restarting the DHCP process, it is still a costly operation if a lot of servers and networks (subnets in Kea) are registered.
The advantages over direct lease reservation are:
* it is possible to associate classes to reservations and finely associate options to some specific hosts (but it is not clear why we would like to use it).
* it does not require a custom plugin and is a well-supported feature.

### Using another DHCP server
ISC Kea does not support the proxyDHCP mode so a solution with dnsmasq is still needed for deployments using this approach.
