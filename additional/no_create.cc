/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <hooks/hooks.h>
#include <dhcpsrv/subnet.h>

using namespace isc;
using namespace isc::hooks;

extern "C" {

int version() {
    return (KEA_HOOKS_VERSION);
}

int lease4_select(CalloutHandle& handle) {
    isc::dhcp::Subnet4Ptr subnet_ptr;
    handle.getArgument("subnet4", subnet_ptr);
    data::ConstElementPtr context_ptr = subnet_ptr->getContext();
    if (context_ptr != NULL && context_ptr->getType() == data::Element::map && context_ptr->contains("no_lease")) {
        handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
    }
    return (0);
}

int load(LibraryHandle& /* handle */) {
    return (0);
}
 
int unload() {
    return (0);
}

int multi_threading_compatible() {
    return (1);
}

}