# Build the manager binary
FROM golang:1.20 as builder
ARG TARGETOS
ARG TARGETARCH

LABEL project=kanod-kea

WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
RUN go mod download

# Copy the go source
COPY api/ api/
COPY cmd/ cmd/
COPY internal/ internal/

# Build
RUN CGO_ENABLED=0 GOOS=${TARGETOS:-linux} GOARCH=${TARGETARCH} go build -a -o manager cmd/main.go

# Build ipxe executables
FROM ubuntu:18.04 as base
LABEL project=kanod-kea

RUN apt-get update && apt-get install -y gcc binutils make perl liblzma-dev mtools git && mkdir /base && cd base && git clone --branch v1.21.1 --depth 1 https://git.ipxe.org/ipxe.git && cd ipxe/src && make -j 4 bin-x86_64-efi/ipxe.efi bin-x86_64-efi/snponly.efi bin/undionly.kpxe

FROM alpine:3.17 as kea-plugin
RUN apk add --no-cache bash curl libcap
RUN curl -1sLf 'https://dl.cloudsmith.io/public/isc/kea-2-4/setup.alpine.sh' | bash
RUN apk add --no-cache isc-kea-dhcp4 isc-kea-hooks isc-kea-dev boost-dev g++
RUN mkdir /no_create
COPY additional/no_create.cc /no_create
RUN cd /no_create && \
    g++ -I /usr/include/kea/ -L /usr/lib -fpic -shared -o no_create.so \
         no_create.cc -lkea-dhcpsrv -lkea-dhcp++ \
		-lkea-hooks -lkea-log -lkea-util -lkea-exceptions

FROM alpine:3.17
ENV IRONIC_GID=994
ENV IRONIC_UID=997
LABEL project=kanod-kea
RUN addgroup -g "${IRONIC_GID}" -S ironic
RUN adduser -G ironic -u "${IRONIC_UID}" -s /sbin/nologin -S -H ironic

COPY --from=builder /workspace/manager .

RUN apk add --no-cache bash curl libcap
RUN curl -1sLf 'https://dl.cloudsmith.io/public/isc/kea-2-4/setup.alpine.sh' | bash
RUN apk update

RUN mkdir /config && apk add --no-cache tftp-hpa isc-kea-ctrl-agent isc-kea-dhcp4 isc-kea-hooks  && \
    adduser -D tftp && mkdir /tftpboot
COPY --from=base /base/ipxe/src/bin-x86_64-efi/ipxe.efi /tftpboot
COPY --from=base /base/ipxe/src/bin-x86_64-efi/snponly.efi /tftpboot
COPY --from=base /base/ipxe/src/bin/undionly.kpxe /tftpboot
COPY --from=kea-plugin /no_create/no_create.so /usr/lib/kea/hooks/no_create.so
COPY additional/kea-dhcp4.tmpl /etc/kea/kea-dhcp4.tmpl

RUN mkdir -p /var/lib/kea
RUN mkdir -p /run/kea
RUN chown -R "ironic:ironic" /etc/kea /tftpboot /run/kea /var/lib/kea /var/log/kea
RUN chmod o+x /usr/sbin/kea-dhcp4 /usr/sbin/kea-ctrl-agent
RUN setcap "cap_net_raw,cap_net_admin,cap_net_bind_service=+eip" /usr/sbin/kea-dhcp4
RUN setcap "cap_net_raw,cap_net_admin,cap_sys_chroot,cap_setuid,cap_setgid,cap_net_bind_service=+eip" /usr/sbin/in.tftpd

WORKDIR /
COPY --from=builder /workspace/manager .

USER ${IRONIC_UID}

ENTRYPOINT ["/manager"]
