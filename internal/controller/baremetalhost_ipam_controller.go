/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"strings"

	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	m3ipam "github.com/metal3-io/ip-address-manager/api/v1alpha1"
	keav1alpha1 "gitlab.com/Orange-OpenSource/kanod/kanod-kea/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	k8slog "sigs.k8s.io/controller-runtime/pkg/log"
)

var EnableBMHNameBasedPreallocation bool

// BareMetalHostReconciler reconciles a BareMetalHost object
type BareMetalHostIpamReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/finalizers,verbs=update
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ippools,verbs=get;list;watch
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ipclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ipaddresses,verbs=get;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the BareMetalHost object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *BareMetalHostIpamReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := k8slog.FromContext(ctx)
	bmh := &bmhv1alpha1.BareMetalHost{}
	err := r.Get(ctx, req.NamespacedName, bmh)
	log.V(1).Info("starting reconcile")
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("baremetalhost resource not found")
			return ctrl.Result{}, nil
		}
		log.Error(err, "failed to get baremetalhost resource")
		return ctrl.Result{}, err
	}
	if bmh.Annotations == nil {
		log.V(1).Info("no annotation")
		return ctrl.Result{}, nil
	}

	if bmh.Annotations[bmhv1alpha1.PausedAnnotation] == "metal3.io/capm3" {
		log.V(1).Info("BareMetalHost paused because of pivot")
		return ctrl.Result{}, nil
	}

	spec := bmh.Annotations[keav1alpha1.IpamAnnotation]
	if spec == "" {
		log.V(1).Info("no ipam annotation")
		return ctrl.Result{}, nil
	}
	specIpam := parseSpec(spec)
	requeue := false
	update := false
	for addrName, poolName := range specIpam {
		var ipamClaim = &m3ipam.IPClaim{}
		var claimName string
		if EnableBMHNameBasedPreallocation {
			claimName = fmt.Sprintf("%s-%s", bmh.Name, poolName)
		} else {
			claimName = fmt.Sprintf("%s-%s", bmh.Name, addrName)
		}

		key := types.NamespacedName{
			Name:      claimName,
			Namespace: bmh.Namespace,
		}
		err := r.Get(ctx, key, ipamClaim)
		if err != nil {
			if k8serrors.IsNotFound(err) {
				err := createM3IPClaim(r.Client, ctx, claimName, bmh, poolName)
				if err != nil {
					log.Error(err, "cannot create IPClaim", "ipclaim", claimName)
					return ctrl.Result{}, err
				}
				requeue = true
				log.V(1).Info("creating ipclaim", "ipclaim", claimName)
				continue
			} else {
				log.Error(err, "cannot get IPClaim", "ipclaim", claimName)
				return ctrl.Result{}, err
			}
		}
		addrRef := ipamClaim.Status.Address
		if addrRef == nil {
			log.V(1).Info("ipclaim has no address yet", "ipclaim", claimName)
			requeue = true
			continue
		}
		var address = &m3ipam.IPAddress{}
		key = types.NamespacedName{
			Name:      addrRef.Name,
			Namespace: addrRef.Namespace,
		}
		err = r.Get(ctx, key, address)
		if err != nil {
			log.Error(err, "cannot access IP Address", "key", addrRef.Name, "ipclaim", claimName)
			return ctrl.Result{}, err
		}
		addr := string(address.Spec.Address)
		addrAnnotation := fmt.Sprintf("%s/%s", keav1alpha1.AddressDomain, addrName)
		var foundAddr string
		if bmh.Annotations != nil {
			foundAddr = bmh.Annotations[addrAnnotation]
		}
		log.V(1).Info("comparing addresses", "old", foundAddr, "new", addr)
		if foundAddr != addr {
			update = true
			bmh.Annotations[addrAnnotation] = addr
		}
	}
	if requeue {
		return ctrl.Result{}, nil
	}
	if update {
		log.V(1).Info("update bmh for ip address")
		err := r.Update(ctx, bmh)
		if err != nil {
			log.Error(err, "cannot update the associated ips")
			return ctrl.Result{}, err
		}
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *BareMetalHostIpamReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		Named("baremetalhost_ipam").
		For(&bmhv1alpha1.BareMetalHost{}).
		Owns(&m3ipam.IPClaim{}).
		Complete(r)
}

func parseSpec(spec string) map[string]string {
	specValue := map[string]string{}
	for _, chunk := range strings.Split(spec, ",") {
		elts := strings.Split(chunk, ":")
		if len(elts) != 2 {
			continue
		}
		specValue[strings.TrimSpace(elts[0])] = strings.TrimSpace(elts[1])
	}
	return specValue
}

func createM3IPClaim(
	cl client.Client,
	ctx context.Context,
	claimName string,
	bmh *bmhv1alpha1.BareMetalHost,
	ippoolName string,
) error {
	namespace := bmh.Namespace
	var ipClaim = &m3ipam.IPClaim{
		ObjectMeta: metav1.ObjectMeta{
			Name:      claimName,
			Namespace: namespace,
			OwnerReferences: []metav1.OwnerReference{
				*metav1.NewControllerRef(bmh, bmhv1alpha1.GroupVersion.WithKind("BareMetalHost")),
			},
		},
		Spec: m3ipam.IPClaimSpec{
			Pool: corev1.ObjectReference{
				Name:      ippoolName,
				Namespace: namespace,
			},
		},
	}
	return cl.Create(ctx, ipClaim)
}
