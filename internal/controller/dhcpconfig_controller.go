/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"bytes"
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"net"
	"os"
	"sort"
	"strconv"
	"sync"
	"text/template"

	"github.com/go-logr/logr"
	m3ipam "github.com/metal3-io/ip-address-manager/api/v1alpha1"
	keav1alpha1 "gitlab.com/Orange-OpenSource/kanod/kanod-kea/api/v1alpha1"
	"gitlab.com/Orange-OpenSource/kanod/kanod-kea/internal/kea"
	"golang.org/x/exp/slices"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	k8slog "sigs.k8s.io/controller-runtime/pkg/log"
)

const (
	DefaultTemplatePath = "/etc/kea/kea-dhcp4.tmpl"
	TemplatePathVar     = "TEMPLATE_PATH"
)

// DhcpConfigReconciler reconciles a DhcpConfig object
type DhcpConfigReconciler struct {
	client.Client
	Scheme      *runtime.Scheme
	KeaTemplate *template.Template
	mutex       sync.Mutex
	Config      KeaConfig
	hash        []byte
}

// KeaConfig is the dynamic part of the configuration of the Kea DHCP4 server
// as used with the template.
type KeaConfig struct {
	IronicIp      string
	IronicPort    int
	ComputedLease bool
	DhcpInterface string
	DhcpServerIp  string
	Subnets       []Subnet
}

// Subnet is the internal representation of the Kea subnet
type Subnet struct {
	Subnet     string
	Prefix     int
	Id         int
	Pools      []keav1alpha1.Pool
	Pxe        bool
	Gateway    string
	DnsServers []string
}

const (
	// PausedAnnotation is the annotation that pauses the reconciliation
	PausedAnnotation string = "dhcpconfig.kanod.io/paused"
)

var (
	EnableComputedLease bool = false
)

//+kubebuilder:rbac:groups=network.kanod.io,resources=dhcpconfigs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=network.kanod.io,resources=dhcpconfigs/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=network.kanod.io,resources=dhcpconfigs/finalizers,verbs=update
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ippools,verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the DhcpConfig object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *DhcpConfigReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := k8slog.FromContext(ctx)
	var dhcpConfig = &keav1alpha1.DhcpConfig{}
	err := r.Get(ctx, req.NamespacedName, dhcpConfig)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("dhcpConfig resource not found")
			return ctrl.Result{}, nil
		}
		log.Error(err, "failed to get dhcpConfig resource")
		return ctrl.Result{}, err
	}

	if dhcpConfig.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted and is handled by this controller.
		// if it does not have a finalizer, add it
		if !controllerutil.ContainsFinalizer(dhcpConfig, keav1alpha1.DhcpFinalizer) {
			controllerutil.AddFinalizer(dhcpConfig, keav1alpha1.DhcpFinalizer)
			if err := r.Update(ctx, dhcpConfig); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		// The object is being deleted
		if controllerutil.ContainsFinalizer(dhcpConfig, keav1alpha1.DhcpFinalizer) {
			if dhcpConfig.Status.SubnetId != nil {
				id := *dhcpConfig.Status.SubnetId
				r.mutex.Lock()
				i := r.findSubnet(id)
				r.mutex.Unlock()
				if i > 0 {
					r.mutex.Lock()
					r.Config.Subnets = append(r.Config.Subnets[:i], r.Config.Subnets[i+1:]...)
					r.mutex.Unlock()
					if err = r.UpdateConfig(log); err != nil {
						log.Error(err, "Failure updating configuration after deletion")
					}
				}
			}
			controllerutil.RemoveFinalizer(dhcpConfig, keav1alpha1.DhcpFinalizer)
			if err := r.Update(ctx, dhcpConfig); err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, nil
	}

	if dhcpConfig.Annotations != nil {
		if _, ok := dhcpConfig.Annotations[PausedAnnotation]; ok {
			log.Info("dhcpconfig controller is paused")
			return ctrl.Result{}, nil
		}
	}

	modified := false
	spec := dhcpConfig.Spec
	if dhcpConfig.Status.SubnetId == nil {
		// New subnet, let us handle it.
		if err := r.CreateSubnet(ctx, log, dhcpConfig); err != nil {
			return ctrl.Result{}, err
		}
		modified = true
	} else {
		modified = r.ReuseSubnet(ctx, log, dhcpConfig)
	}
	ippoolName := dhcpConfig.Spec.IPPool
	if ippoolName != "" {
		ippool := &m3ipam.IPPool{
			ObjectMeta: metav1.ObjectMeta{Name: ippoolName, Namespace: dhcpConfig.Namespace},
		}
		controllerutil.CreateOrUpdate(ctx, r.Client, ippool,
			func() error {
				ippool.Spec.Prefix = dhcpConfig.Spec.Prefix
				ippool.Spec.NamePrefix = "ip"
				if ippool.ObjectMeta.CreationTimestamp.IsZero() {
					pools := make([]m3ipam.Pool, len(spec.Pools))
					for i, dhcpPool := range spec.Pools {
						start := m3ipam.IPAddressStr(dhcpPool.Start)
						end := m3ipam.IPAddressStr(dhcpPool.End)
						pools[i] = m3ipam.Pool{
							Start: &start,
							End:   &end,
						}
					}
					ippool.Spec.Pools = pools
				}
				if spec.Gateway != "" {
					gateway := m3ipam.IPAddressStr(spec.Gateway)
					ippool.Spec.Gateway = &gateway
				}
				if spec.DnsServers != nil {
					dns := make([]m3ipam.IPAddressStr, len(spec.DnsServers))
					for i, srv := range spec.DnsServers {
						dns[i] = m3ipam.IPAddressStr(srv)
					}
					ippool.Spec.DNSServers = dns
				}
				return nil
			},
		)
	}
	if dhcpConfig.Status.DhcpServerIp != r.Config.DhcpServerIp {
		dhcpConfig.Status.DhcpServerIp = r.Config.DhcpServerIp
		if err := r.Status().Update(ctx, dhcpConfig); err != nil {
			log.Error(err, "Cannot update status to set dhcpServerIp", "dhcpServerIp", r.Config.DhcpServerIp)
			return ctrl.Result{}, err
		}
	}
	if modified {
		if err := r.UpdateConfig(log); err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

func (r *DhcpConfigReconciler) CreateSubnet(ctx context.Context, log logr.Logger, dhcpConfig *keav1alpha1.DhcpConfig) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	id := r.getId()
	dhcpConfig.Status.SubnetId = &id
	if err := r.Status().Update(ctx, dhcpConfig); err != nil {
		log.Error(err, "Cannot update status to set id", "id", id)
		return err
	}
	spec := dhcpConfig.Spec
	log.V(1).Info("allocating subnet id", "subnet", spec.Subnet, "prefix", spec.Prefix, "id", id)
	subnet := Subnet{
		Id:         id,
		Subnet:     spec.Subnet,
		Prefix:     spec.Prefix,
		Pools:      spec.Pools,
		Pxe:        spec.Pxe,
		Gateway:    spec.Gateway,
		DnsServers: spec.DnsServers,
	}
	r.Config.Subnets = append(r.Config.Subnets, subnet)
	r.sortSubnets()
	return nil
}

func (r *DhcpConfigReconciler) ReuseSubnet(ctx context.Context, log logr.Logger, dhcpConfig *keav1alpha1.DhcpConfig) bool {
	modified := false
	id := *dhcpConfig.Status.SubnetId
	r.mutex.Lock()
	defer r.mutex.Unlock()
	i := r.findSubnet(id)
	spec := dhcpConfig.Spec
	if i < 0 {
		// This should not occur... updating from previous run ?
		log.V(1).Info("restoring", "subnet", spec.Subnet, "prefix", spec.Prefix, "id", id)
		modified = true
		subnet := Subnet{
			Id:         id,
			Subnet:     spec.Subnet,
			Prefix:     spec.Prefix,
			Pools:      spec.Pools,
			Pxe:        spec.Pxe,
			Gateway:    spec.Gateway,
			DnsServers: spec.DnsServers,
		}
		r.Config.Subnets = append(r.Config.Subnets, subnet)
		r.sortSubnets()
	} else {
		modified = updateSubnet(&r.Config.Subnets[i], &spec)
	}
	return modified
}

func (r *DhcpConfigReconciler) UpdateConfig(log logr.Logger) error {
	file, err := os.Create(kea.KeaDhcp4Conf)
	if err != nil {
		log.Info("Cannot open dhcp4 config for writing")
		return err
	}

	defer func() {
		if err := file.Close(); err != nil {
			log.Error(err, "Bad close of config")
		}
	}()

	hashStream := sha256.New()
	confOut := io.MultiWriter(file, hashStream)
	if r.KeaTemplate == nil {
		err := fmt.Errorf("cannot proceed without template")
		log.Error(err, "No template")
		return err
	}
	err = r.KeaTemplate.Execute(confOut, r.Config)
	if err != nil {
		log.Error(err, "Cannot execute")
		return err
	}
	hash := hashStream.Sum(nil)
	if !bytes.Equal(hash, r.hash) {
		log.V(1).Info("Config has changed. Reload config.")
		result, err := kea.SendKeaCommand("config-reload", nil, log)
		if err != nil {
			log.Error(err, "reload configuration failed (internal)")
			return err
		}
		if result.Result != 0 {
			err = fmt.Errorf("config-reload")
			log.Error(err, "reload configuration failed", "code", result.Result, "explanation", result.Text)
			return err
		}
		r.hash = hash
	} else {
		log.V(1).Info("Config unchanged.")
	}
	return nil
}

// NewDhcpConfigReconciler creates the state of a DHCPConfig reconciler.
//
// The template used is parsed once and for all. If one does not used the
// default template, the path can be modified with an environment variable.
func NewDhcpConfigReconciler(mgr ctrl.Manager) (*DhcpConfigReconciler, error) {
	keaTemplatePath := os.Getenv("TEMPLATE_PATH_VAR")
	if keaTemplatePath == "" {
		keaTemplatePath = DefaultTemplatePath
	}
	keaConfigBytes, err := os.ReadFile(keaTemplatePath)
	if err != nil {
		k8slog.Log.Error(err, "cannot read the configuration template", "path", keaTemplatePath)
		return nil, err
	}
	keaConfigTemplate, err := template.New(keaTemplatePath).Parse(string(string(keaConfigBytes)))
	if err != nil {
		k8slog.Log.Error(err, "cannot parse the configuration template", "path", keaTemplatePath)
		return nil, err
	}
	portSpec := os.Getenv("IRONIC_PORT")
	dhcpInterface := os.Getenv("DHCP_INTERFACE")
	dhcpServerIp := os.Getenv("DHCP_SERVER_IP")
	if dhcpServerIp == "" {
		dhcpServerIp, err = getInterfaceIpAddress(dhcpInterface)
		if err != nil {
			k8slog.Log.Error(err, "cannot get ip address from interface", "interface", dhcpInterface)
			return nil, err
		}
		if dhcpServerIp == "" {
			err = fmt.Errorf("no dhcp server ip")
			return nil, err
		}
	}

	// ironic and kea are on the same node with host networking
	ironicIp := dhcpServerIp
	k8slog.Log.Info("Initialize dhcpconfig reconciler", "port", portSpec, "interface", dhcpInterface, "ironic ip", ironicIp, "dhcp server ip", dhcpServerIp)

	port, err := strconv.Atoi(portSpec)
	if err != nil {
		k8slog.Log.Error(err, "Bad specification for ironic port", "port", portSpec)
	}
	return &DhcpConfigReconciler{
		Client:      mgr.GetClient(),
		Scheme:      mgr.GetScheme(),
		KeaTemplate: keaConfigTemplate,
		Config: KeaConfig{
			DhcpInterface: dhcpInterface,
			IronicIp:      ironicIp,
			DhcpServerIp:  dhcpServerIp,
			IronicPort:    port,
			ComputedLease: EnableComputedLease,
		},
	}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DhcpConfigReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&keav1alpha1.DhcpConfig{}).
		Complete(r)
}

// Check if a subnet with a given id already exists using the fact the slice is sorted by id
func (r *DhcpConfigReconciler) findSubnet(id int) int {
	subnets := r.Config.Subnets
	i := sort.Search(len(subnets), func(i int) bool { return id <= subnets[i].Id })
	if i < len(subnets) && subnets[i].Id == id {
		return i
	}
	return -1
}

// sort the subnet slice by id
func (r *DhcpConfigReconciler) sortSubnets() {
	subnets := r.Config.Subnets
	sort.Slice(subnets, func(i, j int) bool {
		return subnets[i].Id < subnets[j].Id
	})
}

// Get a new subnet id (smallest free one).
// We use the invariant that the slice is sorted in increasing id order.
// As soon as we find a hole in the numbering we can use it without checking
// the remaining elements.
func (r *DhcpConfigReconciler) getId() int {
	var cand, idx int
	idx = 0
	cand = 1
	subnets := r.Config.Subnets
	for {
		if idx >= len(subnets) || subnets[idx].Id > cand {
			break
		}
		cand = subnets[idx].Id + 1
		idx = idx + 1
	}
	return cand
}

func updateSubnet(subnet *Subnet, spec *keav1alpha1.DhcpConfigSpec) bool {
	modified := false
	if subnet.Subnet != spec.Subnet {
		subnet.Subnet = spec.Subnet
		modified = true
	}

	if subnet.Prefix != spec.Prefix {
		subnet.Prefix = spec.Prefix
		modified = true
	}

	if !slices.Equal(subnet.Pools, spec.Pools) {
		subnet.Pools = slices.Clone(spec.Pools)
		modified = true
	}

	if subnet.Gateway != spec.Gateway {
		subnet.Gateway = spec.Gateway
		modified = true
	}
	if subnet.Pxe != spec.Pxe {
		subnet.Pxe = spec.Pxe
		modified = true
	}

	if !slices.Equal(subnet.DnsServers, spec.DnsServers) {
		subnet.DnsServers = slices.Clone(spec.DnsServers)
		modified = true
	}
	return modified
}

func getInterfaceIpAddress(itfName string) (addr string, err error) {
	var (
		itf      *net.Interface
		addrs    []net.Addr
		ipv4Addr net.IP
	)
	if itf, err = net.InterfaceByName(itfName); err != nil {
		return
	}

	if addrs, err = itf.Addrs(); err != nil {
		return
	}

	for _, addr := range addrs {
		if ipv4Addr = addr.(*net.IPNet).IP.To4(); ipv4Addr != nil {
			break
		}
	}

	if ipv4Addr == nil {
		err = fmt.Errorf("interface %s doesn't have an ipv4 address", itfName)
		return
	}

	addr = ipv4Addr.String()

	return
}
