/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"sync"

	"github.com/go-logr/logr"
	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	keav1alpha1 "gitlab.com/Orange-OpenSource/kanod/kanod-kea/api/v1alpha1"
	"gitlab.com/Orange-OpenSource/kanod/kanod-kea/internal/kea"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	k8slog "sigs.k8s.io/controller-runtime/pkg/log"
)

// BareMetalHostReconciler reconciles a BareMetalHost object
type BareMetalHostReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	mutex  sync.Mutex
}

//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts/finalizers,verbs=update
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ippools,verbs=get;list;watch
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ipclaims,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=ipam.metal3.io,resources=ipaddresses,verbs=get;list;watch

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the BareMetalHost object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.15.0/pkg/reconcile
func (r *BareMetalHostReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := k8slog.FromContext(ctx)
	bmh := &bmhv1alpha1.BareMetalHost{}
	// TODO(user): your logic here
	err := r.Get(ctx, req.NamespacedName, bmh)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("bareMetalHost resource not found")
			return ctrl.Result{}, nil
		}
		log.Error(err, "failed to get bareMetalHost resource")
		return ctrl.Result{}, err
	}
	ipAddress := getIpAddress(bmh, log)
	log.Info("Handling BMH", "ip", ipAddress)
	if bmh.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted and is handled by this controller.
		// if it does not have a finalizer, add it
		if ipAddress != "" && !controllerutil.ContainsFinalizer(bmh, keav1alpha1.DhcpFinalizer) {
			controllerutil.AddFinalizer(bmh, keav1alpha1.DhcpFinalizer)
			if err := r.Update(ctx, bmh); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		// The object is being deleted
		if controllerutil.ContainsFinalizer(bmh, keav1alpha1.DhcpFinalizer) {
			hwAddress := bmh.Spec.BootMACAddress
			if hwAddress != "" {
				r.mutex.Lock()
				if EnableComputedLease {
					err = kea.DeleteLease(log, hwAddress)
				}
				r.mutex.Unlock()
				if err != nil {
					return ctrl.Result{}, err
				}
			}

			// remove our finalizer from the list and update it.
			controllerutil.RemoveFinalizer(bmh, keav1alpha1.DhcpFinalizer)
			if err := r.Update(ctx, bmh); err != nil {
				return ctrl.Result{}, err
			}
		}

		// Stop reconciliation as the item is being deleted
		return ctrl.Result{}, nil
	}
	if bmh.Annotations != nil && bmh.Annotations[bmhv1alpha1.PausedAnnotation] == "metal3.io/capm3" {
		log.V(1).Info("BareMetalHost paused because of pivot")
		return ctrl.Result{}, nil
	}
	if ipAddress == "" {
		return ctrl.Result{}, nil
	}
	hwAddress := bmh.Spec.BootMACAddress
	r.mutex.Lock()
	if EnableComputedLease {
		err = kea.AddLease(log, hwAddress, ipAddress, bmh.Name)
	}
	r.mutex.Unlock()
	if err != nil {
		return ctrl.Result{}, err
	}

	if bmh.Annotations != nil && bmh.Annotations[bmhv1alpha1.PausedAnnotation] == keav1alpha1.PausedAnnotationValue {
		if err = r.removePauseAnnotation(bmh, log); err != nil {
			return ctrl.Result{}, err
		}
	}

	err = r.markConfiguration(log, bmh)
	return ctrl.Result{}, err
}

// Get the IP address associated to a BareMetalHost for the PXE interface or
// return ""
func getIpAddress(bmh *bmhv1alpha1.BareMetalHost, log logr.Logger) string {
	labels := bmh.GetLabels()
	annotations := bmh.GetAnnotations()

	if labels == nil || annotations == nil {
		log.V(1).Info("no labels or annotations")
		return ""
	}
	netName, ok := labels[keav1alpha1.DhcpLabel]
	if !ok {
		log.V(1).Info("no dhcp label")
		return ""
	}
	addr, ok := annotations[fmt.Sprintf("%s/%s", keav1alpha1.AddressDomain, netName)]
	if !ok {
		log.V(1).Info("no address")
		return ""
	}
	return addr
}

// SetupWithManager sets up the controller with the Manager.
func (r *BareMetalHostReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		Named("baremetalhost_dhcp").
		For(&bmhv1alpha1.BareMetalHost{}).
		Complete(r)
}

// Directly remove a pause annotation on the bareMetalHost when the value of the pause
// indicates this controller is responsbile for managing the field.
func (r *BareMetalHostReconciler) removePauseAnnotation(bareMetalHost *bmhv1alpha1.BareMetalHost, log logr.Logger) error {
	log.Info("Removing paused annotation from BareMetalHost")
	path := fmt.Sprintf("/metadata/annotations/%s", strings.Replace(bmhv1alpha1.PausedAnnotation, "/", "~1", -1))
	content := []map[string]string{{"op": "remove", "path": path}}
	data, err := json.Marshal(content)
	if err != nil {
		log.Error(err, "Cannot marshal json data")
		return err
	}
	patch := client.RawPatch(types.JSONPatchType, data)
	err = r.Client.Patch(context.Background(), bareMetalHost, patch)
	if err != nil {
		log.Error(err, "Failed to remove paused annotation")
		return err
	}

	return nil
}

// Patch the bareMetalHost to signal a lease has been registered.
// Used to remove pauseAnnotation when the pool waits for a combination of signals.
func (r *BareMetalHostReconciler) markConfiguration(log logr.Logger, bmh *bmhv1alpha1.BareMetalHost) error {
	var present bool
	if bmh.Annotations != nil {
		_, present = bmh.Annotations[keav1alpha1.DhcpMarkerAnnotation]
	}
	if !present {
		path := fmt.Sprintf("/metadata/annotations/%s", strings.Replace(keav1alpha1.DhcpMarkerAnnotation, "/", "~1", -1))
		content := []map[string]string{{"op": "replace", "path": path, "value": "true"}}
		data, err := json.Marshal(content)
		if err != nil {
			log.Error(err, "Cannot marshal json data")
			return err
		}
		patch := client.RawPatch(types.JSONPatchType, data)
		err = r.Client.Patch(context.Background(), bmh, patch)
		if err != nil {
			log.Error(err, "Failed to patch dhcp marker on bare-metal host")
			return err
		}
	}
	return nil
}
