/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kea

import (
	"bytes"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-logr/logr"
)

type KeaCommand struct {
	Command   string      `json:"command"`
	Arguments interface{} `json:"arguments,omitempty"`
	Service   []string    `json:"service,omitempty"`
}

type KeaResult struct {
	Result    int         `json:"result"`
	Text      string      `json:"text"`
	Arguments interface{} `json:"arguments,omitempty"`
}

const (
	Debug = 1
)

func SendKeaCommand(command string, args interface{}, log logr.Logger) (*KeaResult, error) {
	com := KeaCommand{
		Command:   command,
		Service:   []string{"dhcp4"},
		Arguments: args,
	}
	log = log.WithValues("command", command, "arguments", args)
	data := new(bytes.Buffer)
	json.NewEncoder(data).Encode(com)
	sha := sha1.New()
	sha.Write(data.Bytes())
	log.V(Debug).Info("Send command to Kea")
	req, err := http.NewRequest(http.MethodPost, "http://localhost:8000", data)
	if err != nil {
		log.Error(err, "Failed to create Kea command")
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		log.Error(err, "Failed to send Kea command")
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("KeaCommand %d", res.StatusCode)
	}
	decoder := json.NewDecoder(res.Body)
	var body = []KeaResult{}
	err = decoder.Decode(&body)
	if err != nil {
		log.Error(err, "problem fetching body")
		return nil, err
	}

	if len(body) != 1 {
		err := fmt.Errorf("bad length")
		log.Error(err, "malformed result", "results", len(body))
		return nil, err
	}
	result := body[0]
	log.V(1).Info("command returned", "code", result.Result, "results", result.Arguments)
	return &result, nil
}
