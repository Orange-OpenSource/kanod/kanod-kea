/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kea

import (
	"os"
	"os/exec"
	"time"

	"github.com/go-logr/logr"
)

const (
	// Config file for Kea DHCP4 server
	KeaDhcp4Conf = "/etc/kea/kea-dhcp4.conf"
	// Config file for the Kea control agent responsible for updates.
	KeaCtrlAgentConf = "/etc/kea/kea-ctrl-agent.conf"
)

func Launch(log logr.Logger, logname string, command string, args ...string) {
	log.Info("Launching command ", "command", command, "args", args)
	go func() {
		for {
			cmd := exec.Command(command, args...)
			cmd.Stderr = os.Stderr
			cmd.Stdout = os.Stdout
			err := cmd.Run()
			if err != nil {
				log.Error(err, "command failed.")
			}
			time.Sleep(1000 * time.Millisecond)
			log.Info("Restarting command")
		}
	}()
}

func LaunchServices(log logr.Logger) {
	Launch(log, "tftpd",
		"/usr/sbin/in.tftpd", "-L", "-vvv",
		"-u", "ironic", "--secure", "/tftpboot")
	Launch(log, "kea-dhcp4", "/usr/sbin/kea-dhcp4", "-c", KeaDhcp4Conf)
	Launch(log, "kea-ctrl-agent", "/usr/sbin/kea-ctrl-agent", "-c", KeaCtrlAgentConf)
}
