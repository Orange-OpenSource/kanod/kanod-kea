/*
Copyright Orange 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kea

import (
	"fmt"

	"github.com/go-logr/logr"
)

// function for logging leases
// func DebugLeases(log logr.Logger, msg string) {
// 	cmdResult, err := SendKeaCommand("lease4-get-all", nil, log)
// 	if err != nil {
// 		log.Error(err, "cannot get leases")
// 		return
// 	}
// 	args, ok := cmdResult.Arguments.(map[string]interface{})
// 	if !ok {
// 		log.Error(err, "cannot get leases (2)")
// 		return
// 	}
// 	rawLeases, ok := args["leases"]
// 	if !ok {
// 		log.Error(err, "cannot get leases (3)")
// 		return
// 	}
// 	leases, ok := rawLeases.([]interface{})
// 	if !ok {
// 		log.Error(err, "cannot get leases (4)")
// 		return
// 	}
// 	var result strings.Builder
// 	for i, rawLease := range leases {
// 		lease, ok := rawLease.(map[string]interface{})
// 		if !ok {
// 			log.Error(err, "cannot get leases (5)")
// 			continue
// 		}
// 		if i > 0 {
// 			result.WriteString(" - ")
// 		}
// 		result.WriteString(lease["hw-address"].(string))
// 		result.WriteString(": ")
// 		result.WriteString(lease["ip-address"].(string))
// 	}
// 	log.V(1).Info(msg, "leases", result.String())
// }

// AddLease adds a lease in Kea database if necessary.
// This may be just a change of IP address for a given MAC. But the IP cannot
// be allocated to somebody else.
func AddLease(log logr.Logger, hwAddress string, ipAddress string, name string) error {
	// DebugLeases(log, "Before Add Lease")
	args := map[string]string{
		"ip-address": ipAddress,
	}
	result, err := SendKeaCommand("lease4-get", args, log)
	if err != nil {
		log.Error(err, "lease4-get failed")
		return err
	}
	// Lease for IP exists.
	if result.Result == 0 {
		resultMap, ok := result.Arguments.(map[string]interface{})
		if !ok {
			return fmt.Errorf("bad result")
		}
		hwAddressLease, ok := resultMap["hw-address"]
		// Same IP address for two MAC address: there is a conflict.
		// We are not going to steal somebody else IP !
		if ok && hwAddress != hwAddressLease {
			err := fmt.Errorf("conflict")
			log.Error(err, "conflict on lease", "ipAddress", ipAddress, "hwAddress", hwAddress, "old", hwAddressLease)
			return err
		}
	}
	args = map[string]string{
		"hw-address": hwAddress,
	}
	// Check if a lease already exists for this MAC
	result, err = SendKeaCommand("lease4-get-by-hwAddress", args, log)
	if err != nil {
		log.Error(err, "lease4-get-by-hwAddress failed")
		return err
	}
	// The MAC address had a different IP address already. We remove the old lease first.
	if result.Result == 0 {
		result, err = SendKeaCommand("lease4-del", args, log)
		if err != nil {
			log.Error(err, "lease4-del failed")
			return err
		}
		if result.Result != 0 {
			err := fmt.Errorf("lease deletion failed")
			log.Error(err, "lease deletion failed", "text", result.Text, "hwAddress", hwAddress)
			return err
		}
	}
	// Create the lease
	args = map[string]string{
		"hw-address": hwAddress,
		"ip-address": ipAddress,
		"hostname":   name,
	}
	_, err = SendKeaCommand("lease4-add", args, log)
	if err != nil {
		log.Error(err, "lease4-add failed")
	}
	// DebugLeases(log, "After Add Lease")
	return err
}

// DeleteLease deletes a lease in Kea database.
// This must be done through the IP address
func DeleteLease(log logr.Logger, hwAddress string) error {
	// DebugLeases(log, "Before Delete Lease")
	args := map[string]string{
		"hw-address": hwAddress,
	}
	// Get the lease first
	result, err := SendKeaCommand("lease4-get-by-hwAddress", args, log)
	if err != nil {
		log.Error(err, "lease4-get-by-hwAddress failed")
		return err
	}
	if result.Result != 0 {
		return nil
	}
	// Extract the IP address
	resultMap, ok := result.Arguments.(map[string]interface{})
	if !ok {
		return fmt.Errorf("bad result")
	}
	ipAddress, ok := resultMap["ip-address"]
	if !ok {
		return fmt.Errorf("ip address not found in result")
	}
	// Delete the lease through the IP address
	args = map[string]string{
		"ip-address": ipAddress.(string),
	}
	_, err = SendKeaCommand("lease4-del", args, log)
	if err != nil {
		log.Error(err, "lease4-del failed")
	}
	// DebugLeases(log, "After Delete Lease")
	return err
}
