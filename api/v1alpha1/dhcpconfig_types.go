/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// Name of annotation containing the ip addresses to generate and the pool to use.
	// It is a comma separated list of pairs <ip name>: <ippool name>
	IpamAnnotation = "kanod.io/ipam"
	// Name of the label containing the name of ip address to use for DHCP lease
	DhcpLabel = "kanod.io/dhcp"
	// Annotation domain containing ip addresses annotations
	AddressDomain = "ip.kanod.io"
	// finalizer used by the kea dhcp controller to remove lease when host disapear
	DhcpFinalizer = "dhcp.kanod.io/finalizer"
	// value of the paused annotation when kea controller holds the lock.
	PausedAnnotationValue = "kanod.io/kanod-kea"
	// Marker indicating the configuration
	DhcpMarkerAnnotation = "dhcp.kanod.io/configured"
)

// DhcpConfigSpec defines the desired state of DhcpConfig
type DhcpConfigSpec struct {
	// Prefix of the network
	Subnet string `json:"subnet"`
	// Mask size
	Prefix int `json:"prefix,omitempty"`
	// Pools used in that subnet
	Pools []Pool `json:"pools,omitempty"`
	// Whether PXE boot is enabled on the network
	Pxe bool `json:"pxe,omitempty"`
	// Gateway address advertised by DHCP server
	Gateway string `json:"gateway,omitempty"`
	// List of DNS server addresses advertised by DHCP server
	DnsServers []string `json:"dnsServers,omitempty"`
	// IPPool to generate or omit if not used
	IPPool string `json:"ipPool,omitempty"`
}

// Pool define a pool of available addresses within a subnet.
type Pool struct {
	// Range start for address allocation (single range)
	Start string `json:"start,omitempty"`
	// Range end for address allocation (single range)
	End string `json:"end,omitempty"`
}

// DhcpConfigStatus defines the observed state of DhcpConfig
type DhcpConfigStatus struct {
	// Kea id of the subnet.
	SubnetId     *int   `json:"subnetId,omitempty"`
	DhcpServerIp string `json:"dhcpServerIp,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// DhcpConfig is the Schema for the dhcpconfigs API
type DhcpConfig struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DhcpConfigSpec   `json:"spec,omitempty"`
	Status DhcpConfigStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DhcpConfigList contains a list of DhcpConfig
type DhcpConfigList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DhcpConfig `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DhcpConfig{}, &DhcpConfigList{})
}
