# kanod-kea
A controller starting ISC kea and maintaining its configuration according
to the available network definitions and baremetal hosts.

## Description
kanod-kea uses its own custom resource to configure the subnets in the
configuration of the embedded kea.
